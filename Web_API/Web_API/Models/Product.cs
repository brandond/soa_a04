﻿/*
* FILE : Product.cs
* PROJECT : PROG3080 - Assignment #4
* PROGRAMMER : Brandon Davies
* FIRST VERSION : 2017-12-11
* DESCRIPTION :
* This contains the definition for the product data model
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Web_API.Models
{
    public class Product
    {
        [Key]
        public int prodID { get; set; }

        public string prodName { get; set; }

        public float price { get; set; }

        public float prodWeight { get; set; }

        public sbyte inStock { get; set; }
    }
}
