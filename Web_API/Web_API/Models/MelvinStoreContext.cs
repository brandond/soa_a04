﻿/*
* FILE : MelvinStoreContext.cs
* PROJECT : PROG3080 - Assignment #4
* PROGRAMMER : Hayden
* FIRST VERSION : 2017-12-11
* DESCRIPTION :
* This contains the definiton of the database context
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Web_API.Models
{
    public class MelvinStoreContext : DbContext
    {
        public MelvinStoreContext(DbContextOptions<MelvinStoreContext> options) : base(options)
        {

        }

        public DbSet<Customer> Customer { get; set; }

        public DbSet<Product> Product { get; set; }

        public DbSet<Cart> Cart { get; set; }

        public DbSet<Order> Order { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cart>()
                .HasKey(o => new { o.OrderId, o.ProdId });
        }
    }
}
