﻿/*
* FILE : Customer.cs
* PROJECT : PROG3080 - Assignment #4
* PROGRAMMER : Brandon Davies
* FIRST VERSION : 2017-12-11
* DESCRIPTION :
* This contains the definiton for the customer data model
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Web_API.Models
{
    public class Customer
    {
        [Key]
        public int custID { get; set; }

        public string firstName { get; set; }

        public string lastName { get; set; }

        public string phoneNumber { get; set; }
    }
}
