﻿/*
* FILE : CustomerController.cs
* PROJECT : PROG3080 - Assignment #4
* PROGRAMMER : Brandon Davies
* FIRST VERSION : 2017-12-11
* DESCRIPTION :
* This contains the code for the customer specific routes
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Web_API.Models;
using System.Linq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Web_API.Controllers
{
    [Route("api/[controller]")]
    public class CustomerController : Controller
    {
        private readonly MelvinStoreContext _context;
        private Regex phoneRegex = new Regex(@"^([0-9]){3}-([0-9]){3}-([0-9]){4}$");


        public CustomerController(MelvinStoreContext context)
        {
            _context = context;
        }

        // GET: api/Customer        
        /// <summary>
        /// Get route to search for the customer data based on querystring filters.
        /// </summary>
        /// <returns>Either the customer objects the user searched for, or an error if the user provided bad data</returns>
        [HttpGet]
        public IActionResult Get()
        {
            IQueryCollection queryString = Request.Query;
            var search = (from c in _context.Customer select c);

            foreach(var keyPair in queryString)
            {
                switch (keyPair.Key)
                {
                    case "cu-custID":
                        search = search.Where(c => c.custID == Int32.Parse(queryString["cu-custId"]));
                        break;
                    case "cu-firstName":
                        search = search.Where(c => c.firstName == queryString["cu-firstName"]);
                        break;
                    case "cu-lastName":
                        search = search.Where(c => c.lastName == queryString["cu-lastName"]);
                        break;
                    case "cu-phoneNumber":
                        search = search.Where(c => c.phoneNumber == queryString["cu-phoneNumber"]);
                        break;
                    default:
                        //error - invalid query
                        string invalidQueryError = "Invalid query argument: " + keyPair.Key;
                        return BadRequest(invalidQueryError);                   
                        break;
                }
            }
            
            return new ObjectResult(search);
        }

        // GET api/Customer/5        
        /// <summary>
        /// Gets the customer by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The customer object, or an error indicating the object wasn't found</returns>
        [HttpGet("{id}", Name = "getCustomer")]
        public IActionResult GetById(long id)
        {
            var item = _context.Customer.FirstOrDefault(t => t.custID == id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/Customer        
        /// <summary>
        /// Creates the specified customer.
        /// </summary>
        /// <param name="item">The new customer.</param>
        /// <returns>Either an OK status code, or an Error depending on execution results</returns>
        [HttpPost]
        public IActionResult Create([FromBody] Customer item)
        {
            if (item == null)
            {
                return BadRequest("Error: Invalid data specified for Customer");
            }

            if (item.lastName.Length > 50)
            {
                return BadRequest("Error: lastName cannot be longer than 50 characters");
            }
            else if (item.firstName.Length > 50)
            {
                return BadRequest("Error: firstName cannot be longer than 50 characters");
            }
            else if (item.phoneNumber.Length != 12 || !phoneRegex.IsMatch(item.phoneNumber))
            {
                return BadRequest("Error: invalid format of phoneNumber");
            }
            else
            {
                _context.Customer.Add(item);
                _context.SaveChanges();

                return CreatedAtRoute("GetCustomer", new { id = item.custID }, item);
            }
        }

        // PUT api/Customer/5        
        /// <summary>
        /// Updates the specified customer.
        /// </summary>
        /// <param name="id">The identifier of the customer.</param>
        /// <param name="item">The customer's updated values.</param>
        /// <returns>Either an OK status code, or an Error depending on execution results</returns>
        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody] Customer item)
        {
            if (item == null)
            {
                return BadRequest("Error: Invalid data specified in Customer Object");
            }
            else if (item.custID != id)
            {
                return BadRequest("Error: invalid customer ID, please ensure that the ID you specify in the api url is the same ID used in the updated customer"); 
            }

            var customer = _context.Customer.FirstOrDefault(a => a.custID == id);
            if (customer == null)
            {
                return NotFound();
            }

            if (item.lastName.Length > 50)
            {
                return BadRequest("Error: lastName cannot be longer than 50 characters");
            }
            else if (item.firstName.Length > 50)
            {
                return BadRequest("Error: firstName cannot be longer than 50 characters");
            }
            else if (item.phoneNumber.Length != 12 || !phoneRegex.IsMatch(item.phoneNumber))
            {
                return BadRequest("Error: invalid format of phoneNumber");
            }
            else
            {
                customer.firstName = item.firstName;
                customer.lastName = item.lastName;
                customer.phoneNumber = item.phoneNumber;

                _context.Customer.Update(customer);
                _context.SaveChanges();
                return new NoContentResult();
            }
        }

        // DELETE api/Customer/5        
        /// <summary>
        /// Deletes the specified customer.
        /// </summary>
        /// <param name="id">The customer's identifier.</param>
        /// <returns>Either an OK status code, or an Error depending on execution results</returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var customer = _context.Customer.FirstOrDefault(a => a.custID == id);
            if (customer == null)
            {
                return NotFound();
            }

            _context.Customer.Remove(customer);
            _context.SaveChanges();
            return new NoContentResult();
        }
    }
}
