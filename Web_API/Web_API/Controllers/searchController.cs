﻿/*
* FILE : searchController.cs
* PROJECT : PROG3080 - Assignment #4
* PROGRAMMER : Brandon Davies
* FIRST VERSION : 2017-12-11
* DESCRIPTION :
* This contains the code for the search route, allowing for searching accross multiple tables
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Dynamic;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using Web_API.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860



namespace Web_API.Controllers
{

    public class allTables
    {

        public Customer Customer { get; set; }
        public Product Product { get; set; }
        public Order Order { get; set; }
        public Cart Cart { get; set; }

    }

    [Route("api/[controller]")]
    public class searchController : Controller
    {
        private readonly MelvinStoreContext _context;

        public searchController(MelvinStoreContext context)
        {
            _context = context;
        }


        // GET: api/values        
        /// <summary>
        /// The route handler for cross table searching.
        /// </summary>
        /// <returns>the data requested by the user specified by the querystring parameters</returns>
        [HttpGet]
        public IActionResult Get()
        {
            IQueryCollection queryString = Request.Query;

            var searchResult = (from c in _context.Customer
                                join o in _context.Order on c.custID equals o.custId 
                                join d in _context.Cart on o.OrderId equals d.OrderId
                                join p in _context.Product on d.ProdId equals p.prodID
                                select new allTables()
                                {
                                    Customer = c,
                                    Order = o,
                                    Cart = d,
                                    Product = p
                                });

            bool displayCustomer = false;
            bool displayProduct = false;
            bool displayOrder = false;
            bool displayCart = false;

            /* Customer Filters */
            if (queryString.ContainsKey("cu-custID"))
            {
                searchResult = searchResult.Where(c => c.Customer.custID == Int32.Parse(queryString["cu-custID"]));
                displayCustomer = true;
            }

            if (queryString.ContainsKey("cu-firstName"))
            {
                searchResult = searchResult.Where(c => c.Customer.firstName == queryString["cu-firstName"]);
                displayCustomer = true;
            }

            if (queryString.ContainsKey("cu-lastName"))
            {
                searchResult = searchResult.Where(c => c.Customer.lastName == queryString["cu-lastName"]);
                displayCustomer = true;
            }

            if (queryString.ContainsKey("cu-phoneNumber"))
            {
                searchResult = searchResult.Where(c => c.Customer.phoneNumber == queryString["cu-phoneNumber"]);
                displayCustomer = true;
            }

            /* Product Filters */
            if (queryString.ContainsKey("pr-prodID"))
            {
                searchResult = searchResult.Where(p => p.Product.prodID == Int32.Parse(queryString["pr-prodID"]));
                displayProduct = true;
            }

            if (queryString.ContainsKey("pr-prodName"))
            {
                //searchResult = searchResult.Where(p => p.Product.prodName == queryString["pr-prodName"]);
                displayProduct = true;
            }

            if (queryString.ContainsKey("pr-price"))
            {
                searchResult = searchResult.Where(p => p.Product.price == queryString["pr-price"]);
                displayProduct = true;
            }

            if (queryString.ContainsKey("pr-prodWeight"))
            {
                searchResult = searchResult.Where(p => p.Product.prodWeight == queryString["pr-prodWeight"]);
                displayProduct = true;
            }

            if (queryString.ContainsKey("pr-inStock"))
            {
                searchResult = searchResult.Where(p => p.Product.inStock == SByte.Parse(queryString["pr-inStock"]));
                displayProduct = true;
            }

            /* Order Filters */
            if (queryString.ContainsKey("or-orderID"))
            {
                searchResult = searchResult.Where(o => o.Order.OrderId == Int32.Parse(queryString["or-orderID"]));
                displayOrder = true;
            }

            if (queryString.ContainsKey("or-custID"))
            {
                searchResult = searchResult.Where(o => o.Order.custId == Int32.Parse(queryString["or-custID"]));
                displayOrder = true;
            }

            if (queryString.ContainsKey("or-poNumber"))
            {
                searchResult = searchResult.Where(o => o.Order.PoNumber == queryString["or-poNumber"]);
                displayOrder = true;
            }

            if (queryString.ContainsKey("or-orderDate"))
            {
                searchResult = searchResult.Where(o => o.Order.OrderDate == queryString["or-orderDate"]);
                displayOrder = true;
            }

            /* Cart Filters */
            if (queryString.ContainsKey("ca-orderID"))
            {
                searchResult = searchResult.Where(c => c.Cart.OrderId == Int32.Parse(queryString["ca-orderID"]));
                displayCart = true;
            }

            if (queryString.ContainsKey("ca-prodID"))
            {
                searchResult = searchResult.Where(c => c.Cart.ProdId == Int32.Parse(queryString["ca-prodID"]));
                displayCart = true;
            }

            if (queryString.ContainsKey("ca-quantity"))
            {
                searchResult = searchResult.Where(c => c.Cart.quantity == queryString["ca-quantity"]);
                displayCart = true;
            }

            if (queryString.ContainsKey("generatePO"))
            {
                displayCustomer = true;
                displayOrder = true;
                displayProduct = true;
                displayCart = true;
            }

            List<allTables> newTables = new List<allTables>();
            allTables newAllTables;

            foreach (var result in searchResult)
            {
                newAllTables = new allTables();
                if (displayCustomer)
                {
                    newAllTables.Customer = result.Customer;
                }

                if (displayProduct)
                {
                    newAllTables.Product = result.Product;
                }

                if (displayOrder)
                {
                    newAllTables.Order = result.Order;
                }

                if (displayCart)
                {
                    newAllTables.Cart = result.Cart;
                }
                newTables.Add(newAllTables);

            }

            return new ObjectResult(newTables);


        }
    }
}
