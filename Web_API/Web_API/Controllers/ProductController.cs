﻿/*
* FILE : ProductController.cs
* PROJECT : PROG3080 - Assignment #4
* PROGRAMMER : Brandon Davies
* FIRST VERSION : 2017-12-11
* DESCRIPTION :
* This contains the code for the product specific routes
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Web_API.Models;
using System.Linq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Web_API.Controllers
{
    [Route("api/[controller]")]
    public class ProductController : Controller
    {
        private readonly MelvinStoreContext _context;

        public ProductController(MelvinStoreContext context)
        {
            _context = context;
        }

        // GET: api/values
        /// <summary>
        /// Get route to search for the product data based on querystring filters.
        /// </summary>
        /// <returns>Either the product objects the user searched for, or an error if the user provided bad data</returns>
        [HttpGet]
        public IActionResult Get()
        {
            IQueryCollection queryString = Request.Query;
            var search = (from p in _context.Product select p);

            foreach (var keyPair in queryString)
            {
                switch (keyPair.Key)
                {
                    case "pr-prodID":
                        search = search.Where(p => p.prodID == Int32.Parse(queryString["pr-prodID"]));
                        break;
                    case "pr-prodName":
                        search = search.Where(p => p.prodName == queryString["pr-prodName"]);
                        break;
                    case "pr-price":
                        search = search.Where(p => p.price == float.Parse(queryString["pr-price"]));
                        break;
                    case "pr-prodWeight":
                        search = search.Where(p => p.prodWeight == float.Parse(queryString["pr-prodWeight"]));
                        break;
                    case "pr-inStock":
                        search = search.Where(p => p.inStock == sbyte.Parse(queryString["pr-inStock"]));
                        break;
                    default:
                        //error - invalid query
                        return BadRequest("Invalid query argument - " + keyPair.Key);
                        break;
                }
            }

            return new ObjectResult(search);
        }

        // GET api/values/5        
        /// <summary>
        /// Gets the product by it's identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Either product searched for, or an Error depending on execution results</returns> 
        [HttpGet("{id}", Name = "GetProduct")]
        public IActionResult GetById(long id)
        {
            var item = _context.Product.FirstOrDefault(t => t.prodID == id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/values
        [HttpPost]
        public IActionResult Create([FromBody] Product item)
        {
            if (item == null)
            {
                return BadRequest("Error: Invalid data provided in Cart object to create");
            }

            if (item.prodName.Length > 100)
            {
                return BadRequest("Error: prodName can't be longer than 100 characters");
            }
            else
            {
                _context.Product.Add(item);
                _context.SaveChanges();

                return CreatedAtRoute("GetProduct", new { id = item.prodID }, item);
            }
        }

        // PUT api/values/5        
        /// <summary>
        /// Updates the specified product.
        /// </summary>
        /// <param name="id">The product's identifier.</param>
        /// <param name="item">The item.</param>
        /// <returns>Either an OK status code, or an Error depending on execution results</returns>
        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody] Product item)
        {
            if (item == null)
            {
                return BadRequest("Error: invalid data provided in Cart object to update");
            }
            else if (item.prodID != id)
            {
                return BadRequest("Error: ID in the url must match the ID in the Cart object to update");
            }

            var product = _context.Product.FirstOrDefault(a => a.prodID == id);
            if (product == null)
            {
                return NotFound();
            }

            if (item.prodName.Length > 100)
            {
                return BadRequest("Error: prodName can't be longer than 100 characters");
            }
            else
            {
                product.prodWeight = item.prodWeight;
                product.prodName = item.prodName;
                product.price = item.price;
                product.inStock = item.inStock;

                _context.Product.Update(product);
                _context.SaveChanges();
                return new NoContentResult();
            }
        }

        // DELETE api/values/5        
        /// <summary>
        /// Deletes the specified product.
        /// </summary>
        /// <param name="id">The product's identifier.</param>
        /// <returns>Either an OK status code, or an Error depending on execution results</returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var product = _context.Product.FirstOrDefault(a => a.prodID == id);
            if (product == null)
            {
                return NotFound();
            }

            _context.Product.Remove(product);
            _context.SaveChanges();
            return new NoContentResult();
        }
    }
}
