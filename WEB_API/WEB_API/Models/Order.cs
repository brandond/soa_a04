﻿/*
* FILE : Order.cs
* PROJECT : PROG3080 - Assignment #4
* PROGRAMMER : Brandon Davies
* FIRST VERSION : 2017-12-11
* DESCRIPTION :
* This contains the definition for the order data model
*/
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Web_API.Models
{
    public class Order
    {
        [Key]
        public int OrderId { get; set; }

        public int custId { get; set; }

        public string PoNumber { get; set; }

        public string OrderDate { get; set; }
    }
}
