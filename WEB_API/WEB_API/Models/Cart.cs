﻿/*
* FILE : Cart.cs
* PROJECT : PROG3080 - Assignment #4
* PROGRAMMER : Hayden Taylor
* FIRST VERSION : 2017-12-11
* DESCRIPTION :
* This contains the definition for the cart data model
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web_API.Models
{
    public class Cart
    {
        public int OrderId { get; set; }

        public int ProdId { get; set; }

        public int quantity { get; set; }
    }
}
