﻿/*
* FILE : OrderController.cs
* PROJECT : PROG3080 - Assignment #4
* PROGRAMMER : Hayden Taylor
* FIRST VERSION : 2017-12-11
* DESCRIPTION :
* This contains the code for the order specific routes
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Web_API.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Web_API.Controllers
{
    [Route("api/[controller]")]
    public class OrderController : Controller
    {
        private readonly MelvinStoreContext _context;
        private Regex dateRegex = new Regex(@"^([0-9]){4}-([0-9]){2}-([0-9]){2}$");

        public OrderController(MelvinStoreContext context)
        {
            _context = context;
        }

        // GET: api/values
        /// <summary>
        /// Get route to search for the order data based on querystring filters.
        /// </summary>
        /// <returns>Either the order objects the user searched for, or an error if the user provided bad data</returns>
        [HttpGet]
        public IActionResult GetAll()
        {
            IQueryCollection queryString = Request.Query;
            var search = (from o in _context.Order select o);


            foreach (var keyPair in queryString)
            {
                switch (keyPair.Key)
                {
                    case "or-orderID":
                        search = search.Where(o => o.OrderId == Int32.Parse(queryString["or-orderID"]));
                        break;
                    case "or-custID":
                        search = search.Where(o => o.custId == Int32.Parse(queryString["or-custID"]));
                        break;
                    case "or-orderDate":
                        search = search.Where(o => o.OrderDate == queryString["or-orderDate"]);
                        break;
                    case "or-poNumber":
                        search = search.Where(o => o.PoNumber == queryString["or-poNumber"]);
                        break;
                    default:
                        //error - invalid query
                        return BadRequest("Invalid query argument - " + keyPair.Key);
                        break;
                }
            }

            return new ObjectResult(search);
        }

        // GET api/values/5        
        /// <summary>
        /// Gets the order by it's identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Either an order object, or an Error depending on execution results</returns>
        [HttpGet("{id}", Name = "GetOrder")]
        public IActionResult GetById(long id)
        {
            var item = _context.Order.FirstOrDefault(t => t.OrderId == id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/values        
        /// <summary>
        /// Creates the specified order.
        /// </summary>
        /// <param name="item">The order.</param>
        /// <returns>Either an OK status code, or an Error depending on execution results</returns>
        [HttpPost]
        public IActionResult Create([FromBody]Order item)
        {
            if(item == null)
            {
                return BadRequest("Error: Invalid data specified in Order object");
            }

            if (item.PoNumber.Length > 30)
            {
                return BadRequest("Error: PoNumber can't be larger than 30 characters");
            }
            else if (item.OrderDate.Length != 10 || !dateRegex.IsMatch(item.OrderDate))
            {
                return BadRequest("Error: invalid format for orderDate");
            }
            else
            {
                _context.Order.Add(item);
                _context.SaveChanges();
                return CreatedAtRoute("GetOrder", new { id = item.OrderId }, item);
            }
        }

        // PUT api/values/5        
        /// <summary>
        /// Updates the specified order.
        /// </summary>
        /// <param name="id">The order's identifier.</param>
        /// <param name="item">The order's new values.</param>
        /// <returns>Either an OK status code, or an Error depending on execution results</returns>
        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody]Order item)
        {
            if (item == null)
            {
                return BadRequest("Error: Inavlid data specified in Order object");
            }
            else if (item.OrderId != id)
            {
                return BadRequest("Error: Id of the url must match the ID specified in the object to update");
            }

            var order = _context.Order.FirstOrDefault(o => o.OrderId == id);
            if (order == null)
            {
                return NotFound();
            }

            if (item.PoNumber.Length > 30)
            {
                return BadRequest("Error: PoNumber can't be larger than 30 characters");
            }
            else if (item.OrderDate.Length != 10 || !dateRegex.IsMatch(item.OrderDate))
            {
                return BadRequest("Error: invalid format for orderDate");
            }
            else
            {
                order.custId = item.custId;
                order.OrderDate = item.OrderDate;
                order.PoNumber = item.PoNumber;

                _context.Order.Update(order);
                _context.SaveChanges();
                return new NoContentResult();
            }
        }

        // DELETE api/values/5        
        /// <summary>
        /// Deletes the specified order.
        /// </summary>
        /// <param name="id">The order's identifier.</param>
        /// <returns>Either an OK status code, or an Error depending on execution results</returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var order = _context.Order.FirstOrDefault(o => o.OrderId == id);
            if (order == null)
            {
                return NotFound();
            }

            _context.Order.Remove(order);
            _context.SaveChanges();
            return new NoContentResult();
        }
    }
}
