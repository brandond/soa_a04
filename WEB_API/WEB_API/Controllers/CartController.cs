﻿/*
* FILE : CartController.cs
* PROJECT : PROG3080 - Assignment #4
* PROGRAMMER : Hayden Taylor
* FIRST VERSION : 2017-12-11
* DESCRIPTION :
* This contains the code for the cart specific routes
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Web_API.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Web_API.Controllers
{
    [Route("api/[controller]")]
    public class CartController : Controller
    {

        private readonly MelvinStoreContext _context;

        public CartController(MelvinStoreContext context)
        {
            _context = context;
        }

        // GET: api/values
        /// <summary>
        /// Get route to search for the cart data based on querystring filters.
        /// </summary>
        /// <returns>Either the cart objects the user searched for, or an error if the user provided bad data</returns>
        [HttpGet]
        public IActionResult Get()
        {
            IQueryCollection queryString = Request.Query;
            var search = (from c in _context.Cart select c);

            foreach (var keyPair in queryString)
            {
                switch (keyPair.Key)
                {
                    case "ca-orderID":
                        search = search.Where(c => c.OrderId == Int32.Parse(queryString["ca-orderID"]));
                        break;
                    case "ca-prodID":
                        search = search.Where(c => c.ProdId == Int32.Parse(queryString["ca-prodID"]));
                        break;
                    case "ca-quantity":
                        search = search.Where(c => c.quantity == Int32.Parse(queryString["ca-quantity"]));
                        break;
                    default:
                        //error - invalid query
                        return BadRequest("Invalid query argument - " + keyPair.Key);
                        break;
                }
            }

            return new ObjectResult(search);
        }

        // POST api/values        
        /// <summary>
        /// Creates the specified cart.
        /// </summary>
        /// <param name="cart">The cart.</param>
        /// <returns>Either an OK status code, or an Error depending on execution results</returns>
        [HttpPost]
        public IActionResult Create([FromBody]Cart cart)
        {
            if (cart == null)
            {
                return BadRequest("Error: Invalid data within Cart object");
            }

            _context.Cart.Add(cart);
            _context.SaveChanges();

            //return CreatedAtRoute("GetAlbum", new { id = item.Id }, item);
            return new ObjectResult(cart);
        }

        // PUT api/Cart           
        /// <summary>
        /// Updates the specified cart.
        /// </summary>
        /// <param name="Cart">The cart.</param>
        /// <returns>Either an OK status code, or an Error depending on execution results</returns>
        [HttpPut]
        public IActionResult Update([FromBody]Cart Cart)
        {
            if(Cart == null)
            {
                return BadRequest("Error: Invalid data specified in Cart object");
            }

            var cart = _context.Cart.FirstOrDefault(c => c.OrderId == Cart.OrderId && c.ProdId == Cart.ProdId);

            if(cart == null)
            {
                return NotFound();
            }

            cart.quantity = Cart.quantity;

            _context.Cart.Update(cart);
            _context.SaveChanges();
            return new NoContentResult();
        }

        // DELETE api/Cart?ca-orderID=5&ca-prodID=2        
        /// <summary>
        /// Deletes the specified cart.
        /// </summary>
        /// <returns>Either an OK status code, or an Error depending on execution results</returns>
        [HttpDelete]
        public IActionResult Delete()
        {
            IQueryCollection queryString = Request.Query;
            if (queryString.ContainsKey("ca-orderID") && queryString.ContainsKey("ca-prodID"))
            {
                var cart = _context.Cart.FirstOrDefault(c => c.OrderId == queryString["ca-orderID"] && c.ProdId == queryString["ca-prodID"]);
                if (cart == null)
                {
                    return NotFound();
                }
                _context.Cart.Remove(cart);
                _context.SaveChanges();
                return new NoContentResult();
            }
            else
            {
                return BadRequest("Error: OrderID and ProductID are required to delete a cart object");
            }

            var customer = _context.Customer.First();

        }
    }
}
